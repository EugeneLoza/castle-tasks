{ Copyright (C) 2012-2019 Michalis Kamburelis, Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{---------------------------------------------------------------------------}

(* Crop of Castle Game Engine Time utilities plus a threaded timer.
   Provides for accurate and fast time access, hopefully thread-safe :)
   GetNow should be used to access immediate time value.
   Everywhere possible, DecoNow and DecoNowWorld should be used (updated once per frame).
   GetNowThread and ForceGetNowThread are very specific and should be used
   only in extremely time-critical routines often calculating time like World.Manage *)

{$INCLUDE compilerconfig.inc}

unit DecoTime;

{$DEFINE UseFloatTimer}

interface

uses
  {$ifdef MSWINDOWS} Windows, {$endif}
  {$ifdef UNIX} Unix, {$endif}
  CastleTimeUtils,
  DecoGlobal;

type
  DTime = TFloatTime;
  {see note for CastleTimeUtils.TTimerResult}
  DIntTime = int64;

  DThreadedTime = {$IFDEF UseFloatTimer}DTime{$ELSE}DIntTime{$ENDIF};

const
  { Some ridiculosly large soft pause (equals permanent)
    = over 3 years (with still 7-8 significant digits as DTime = Double, i.e. valid) }
  InfiniteSoftPause: DTime = 1E+8;

var
  { Analogue to Now function, but a fast-access variable, representing
    current global time (time where animations take place)
    works ~200 times faster than SysUtils.Now so should be used anywhere possible
    Updated once per frame}
  DecoNow: DTime;
  DeltaT: DTime;
  { Analogue to Now function, but a fast-access variable, representing
    current in-game time (time where actions take place)
    Updated once per frame }
  DecoNowWorld: DTime;
  DeltaTWorld: DTime;
  { Same for animations
    Animations can run semi-independent of World and Interface time }
  DecoNowAnimation: DTime;
  DeltaTAnimation: DTime;
  { Start time of this frame (in threaded units) to be used by DecoTasks and the like }
  FrameStart: DThreadedTime;
  { SoftPause is a pause when the animations still flow, but World time doesn't move forward
    Gives time in seconds how long the delay should be }
  SoftPause: DTime = 0.0;
  { WorldTime slow by SoftPause, it'll recover slowly afterwards }
  SoftPauseCoefficient: DFloat = 1.0;
  { Global speed of World time flow }
  WorldTimeFlowSpeed: DFloat = 1.0;
  { Global speed of Actors animation time flow }
  AnimationTimeFlowSpeed: DFloat = 1.0;

{ Advance time for the frame }
procedure doTime;
{ Requests a soft-pause (animations run, but actors do not preform actions) }
procedure RequestSoftPauseByAction(const PauseSeconds: DTime);
{ Gets Timer value from some "starting point" in a thread-safe way }
function GetNow: DTime; TryInline
function GetNowInt: DIntTime; TryInline
{ This is a less accurate but accelerated (~130 times) version
  of the timer by using threads. Should be used after ForceGetNowThread.
  Should be used only in time-critical cases, such as World.Manage }
function GetNowThread: DThreadedTime; TryInline
{ Forces initialization of the threaded timer value. }
function ForceGetNowThread: DThreadedTime; TryInline

{ Returns a nice date and time as a String (e.g. for naming files) }
function NiceDate: String;
{ Reset time to safe values }
procedure ResetTime;
{ Call this when game is started/loaded/saved, resets time since last save }
procedure ResetUnsavedGameTime;
{ Was the las save/load too long ago? i.e. = is there essential game progress to loose? }
function SaveWasTooLongAgo: Boolean;
{ Initialize Time routines/classes }
procedure InitTime;
{ Free Time routines/classes }
procedure FreeTime;
{.......................................................................}
implementation

uses
  SysUtils, Classes, {$IFDEF Windows}SyncObjs,{$ENDIF}
  DecoLog {%Profiler%};

function NiceDate: String;
begin
  Result := FormatDateTime('YYYY-MM-DD_hh-nn-ss', Now); //the only place where I'm using SysUtils.Now
end;

{----------------------------------------------------------------------------}

var
  LastGlobalTime: DTime = -1;

const
  WorldTimeRestoreSpeed = 1 / 2; {requires 2 seconds to restore to normal time speed}

procedure doTime;
begin
  {StartProfiler}

  DecoNow := GetNow;
  if LastGlobalTime = -1 then
    LastGlobalTime := DecoNow;
  DeltaT := DecoNow - LastGlobalTime;

  // Calculate World time (affected by SoftPause and WorldTimeFlowSpeed)
  if (SoftPause < DeltaT) then
  begin
    SoftPause := -1;
    DeltaTWorld := DeltaT * WorldTimeFlowSpeed;
    DecoNowWorld += DeltaTWorld;
    //if world time is slowed, then accelerate it softly
    if WorldTimeFlowSpeed < 1 then
    begin
      WorldTimeFlowSpeed += WorldTimeRestoreSpeed * DeltaT;
      if WorldTimeFlowSpeed > 1 then
        WorldTimeFlowSpeed := 1;
    end;
  end
  else
  begin
    //if softpause is issued, then don't perform any actions in world time
    SoftPause -= DeltaT;
    DeltaTWorld := 0;
  end;

  // Calculate animation time

  DeltaTAnimation := DeltaT * AnimationTimeFlowSpeed;
  DecoNowAnimation += DeltaT;

  LastGlobalTime := DecoNow;

  {this gets threaded time for start of the frame
   and also forces initialization of the timer thread,
   because if not initialized frequently enough it may return wrong values.
   well, nothing critical may happen, but let's be on a safe side}
  FrameStart := ForceGetNowThread;

  {StopProfiler}
end;

{----------------------------------------------------------------------------}

procedure ResetTime;
begin
  DecoNow := GetNow;
  LastGlobalTime := DecoNow;
  DeltaT := 0;
  DeltaTWorld := 0;
  //DecoNowWorld += DeltaTWorld;
  DeltaTAnimation := 0;
  //DecoNowAnimation += DeltaT;
  FrameStart := ForceGetNowThread;
end;

{----------------------------------------------------------------------------}

var
  { Last World time when the game was saved/loaded }
  LastSaveWorldTime: DTime;
  { Last Interface time when the game was saved/loaded }
  LastSaveInterfaceTime: DTime;

const
  { 2 minutes of gameplay considered an essential progress requiring confirmation on quit }
  WorldTimeUnsavedProgress = 2 * 60; {seconds}
  { 3 minutes of working with interface considered an essential progress requiring confirmation on quit }
  InterfaceTimeUnsavedProgress = 3 * 60; {seconds}

procedure ResetUnsavedGameTime;
begin
  LastSaveInterfaceTime := DecoNow;
  LastSaveWorldTime := DecoNowWorld;
end;

function SaveWasTooLongAgo: Boolean;
begin
  Result := (DecoNow - LastSaveInterfaceTime > InterfaceTimeUnsavedProgress) or
    (DecoNowWorld - LastSaveWorldTime > WorldTimeUnsavedProgress);
end;
{----------------------------------------------------------------------------}

procedure RequestSoftPauseByAction(const PauseSeconds: DTime);
begin
  SoftPause := PauseSeconds * SoftPauseCoefficient;
  {request PauseSeconds seconds of pause for some animations}
  WorldTimeFlowSpeed := 0; {and slow down world time for next ~2 seconds}
end;

{================================= TIMERS ===================================}

{$IFDEF Windows}
{************************* WINDOWS TIME **************************************}
type
  TTimerFrequency = int64;
  TTimerState = (tsQueryPerformance, tsGetTickCount64);

var
  FTimerState: TTimerState;
  TimerFrequency: TTimerFrequency;
  TimerLock: TCriticalSection;  //we'll need a critical section as we access FTimerState.

function Timer: DIntTime; TryInline
var
  QueryPerformance: Boolean;
begin
  TimerLock.Acquire;   //maybe, this is redundant, but let it be here for now...
  QueryPerformance := FTimerState = tsQueryPerformance;
  TimerLock.Release;

  if QueryPerformance then
    QueryPerformanceCounter(
{$hints off}
      Result
{$hints on}
      )
  else
    {in case of ancient Windows version fall back to GetTickCount :)}
    Result :=
{$warnings off}
      GetTickCount64
{$warnings on}
  ;
end;

{$ENDIF}

{$IFDEF Unix}
{************************* UNIX TIME **************************************}

type
  TTimerFrequency = longword;

const
  TimerFrequency: TTimerFrequency = 1000000;

function Timer: DIntTime; TryInline
var
  tv: TTimeval;
begin
  FpGettimeofday(@tv, nil);
  Result := int64(tv.tv_sec) * 1000000 + int64(tv.tv_usec);
  {overflows may occur here, as Thaddy@LazarusForum suggests}
end;

{$ENDIF}

{============================= GET TIME DIRECTLY =============================}

function GetNow: DTime; TryInline
begin
  Result := Timer / TimerFrequency;
end;

function GetNowInt: DIntTime; TryInline
begin
  Result := Timer;
end;

{========================== GET TIME IN A THREAD =============================}

type
  TTimerThread = class(TThread)
  protected
    procedure Execute; override;
  public
    Time: DIntTime;
  end;

var
  ThreadedTimer: TTimerThread;

procedure TTimerThread.Execute;
begin
  Time := Timer;
end;

{----------------------------------------------------------------------------}

var
  LastTime: DThreadedTime;

function GetNowThread: DThreadedTime; TryInline
begin
  if ThreadedTimer.Finished then
  begin
    LastTime := ThreadedTimer.Time{$IFDEF UseFloatTimer} / TimerFrequency{$ENDIF};
    ThreadedTimer.Start;
  end;
  Result := LastTime;
end;

{----------------------------------------------------------------------------}

{ This procedure forces correct initialization of ThreadedTimer
  and must always be used once before starting accessing the GetNowThread sequentially
  so that the first value will be correct (otherwise it might be extermely wrong,
  which is bad for World.Manage) }
function ForceGetNowThread: DThreadedTime; TryInline
begin
  if ThreadedTimer.Finished then
  begin
    LastTime := {$IFDEF UseFloatTimer}GetNow{$ELSE}GetNowInt{$ENDIF};
    ThreadedTimer.Start;
  end;
  Result := LastTime;
end;

{.............................................................................}

{$IFDEF Windows}
procedure InitWindowsTimer;
begin
  if QueryPerformanceFrequency(TimerFrequency) then
    FTimerState := tsQueryPerformance
  else
  begin
    FTimerState := tsGetTickCount64;
    TimerFrequency := 1000;
  end;
end;
{$ENDIF}

procedure InitTime;
begin
  //create threaded timer and run it immediately to make sure everything is initialized properly
  ThreadedTimer := TTimerThread.Create(true);
  ThreadedTimer.Priority := tpLower;
  ThreadedTimer.FreeOnTerminate := false;
  //ForceGetNowThread;
  FrameStart := -1;

  //Caution, this doesn't look too well
  DecoNowWorld := 0;
  DecoNowAnimation := 0;

  {$IFDEF Windows}
  //initialize the timer in Windows and determine TimerFrequency
  InitWindowsTimer;
  TimerLock := TCriticalSection.Create;
  {$ENDIF}

  doTime; //init all timer variables
end;

{------------------------------------------------------------------------------}

procedure FreeTime;
begin
  ThreadedTimer.Free;
  {$IFDEF Windows}
  TimerLock.Free;
  {$ENDIF}
end;

end.
